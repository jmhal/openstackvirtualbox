#!/bin/bash
USUARIO=$1
SERVICO=$2
DESCRICAO=$3
URL=$4

# Carregar credenciais
. ~/admin-openrc

# Criar Usuário
openstack user create --domain default --password-prompt $USUARIO

# Adicionar papel
openstack role add --project service --user $USUARIO admin

# Criar Serviço
openstack service create --name $USUARIO --description "$DESCRICAO" $SERVICO

# Adicionar os endpoints
openstack endpoint create --region RegionOne $SERVICO public $URL
openstack endpoint create --region RegionOne $SERVICO internal $URL
openstack endpoint create --region RegionOne $SERVICO admin $URL
